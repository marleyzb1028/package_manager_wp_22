const log4js = require('log4js'); //como import en java

let logger = log4js.getLogger();

logger.level = "debug";
//let marley = "Prueba de lint";

logger.info("La aplicación inició correctamente");
logger.warn("Cuidado, falta la libreria x en el sistema");
logger.error("No se encontró la función enviar email");
logger.fatal("No se pudo iniciar la aplicación");

function sumar(a, b){
    return a + b;
}

module.exports = sumar;