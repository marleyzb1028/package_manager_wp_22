const sumar = require('../index');
const assert = require('assert'); //assert = afirmación

describe("Probar la suma de 2 números", ()=>{
    it("5 + 5 = 10", ()=>{ //afirmar
        assert.equal(10, sumar(5, 5));
    });
    it("5 + 7 != 10", ()=>{ 
        assert.notEqual(10, sumar(5, 7));
    });
});